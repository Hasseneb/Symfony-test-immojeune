// Data Table JavaScripts

(function ($) {
	'use strict';
    
	$('#dt-opt').DataTable({
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/French.json"
        }
	});

})(jQuery);
