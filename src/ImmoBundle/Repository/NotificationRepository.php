<?php

namespace ImmoBundle\Repository;

/**
 * NotificationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class NotificationRepository extends \Doctrine\ORM\EntityRepository
{
    public function getAllEvents($date)
    {
             return $this->createQueryBuilder('p')
                 ->andWhere('p.start > :date')
                 ->setParameter('date',$date)
                 ->orderBy('p.start',' ASC')
                 ->setMaxResults(5)
                 ->getQuery()
            ->getResult();
    }
}
