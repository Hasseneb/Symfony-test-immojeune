<?php

namespace ImmoBundle\Form;

use PictureBundle\Form\DossierMedicalType;
use PictureBundle\Form\PictureType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonnelType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('fonction',null,[
            'multiple' => false
        ])->add('grade')
            ->add('dateEmbauche',null,[
                'widget' => 'single_text'
            ])
            ->add('dateFinContrat',null,[
                'widget' => 'single_text'
            ])
            ->add('photo',PictureType::class)
            ->add('dossierMedical',DossierMedicalType::class)
            ->add('salaire')->add('nom')->add('cin')->add('numero')->add('numeroCnss')->add('prenom');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'ImmoBundle\Entity\Personnel',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'immobundle_personnel';
    }


}
