<?php

namespace ImmoBundle\Form;

use PictureBundle\Form\PieceJointeFactureClientType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FactureClientEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type')
            ->add('numeroCommande')
            ->add('nomModel')
            ->add('montant')
            ->add('status',ChoiceType::class,[
                'choices'  => array(
                    'Payée' => true,
                    'Non Payée' => false,
                )
            ])
            ->add('pieceJointe',PieceJointeFactureClientType::class)
            ->add('quantite')
            ->add('datePrevuDencaissement',null,[
                'widget' => 'single_text'
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ImmoBundle\Entity\FactureClient'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'immobundle_factureclient';
    }


}
