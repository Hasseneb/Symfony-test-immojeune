<?php

namespace ImmoBundle\Form;

use PictureBundle\Form\PictureType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FactureFournisseurEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('numeroCommande')->add('nomFournisseur')
            ->add('montant')->add('referenceArticle')
            ->add('status',ChoiceType::class,[
                'choices'  => array(
                    'Payée' => true,
                    'Non Payée' => false,
                )
            ])
            ->add('photo',PictureType::class)
            ->add('dateFacturation',null,[
                'widget' => 'single_text'
            ])->add('datePaiement',null,[
                'widget' => 'single_text'
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ImmoBundle\Entity\FactureFournisseur'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'immobundle_facturefournisseur';
    }


}
