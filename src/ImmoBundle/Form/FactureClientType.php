<?php

namespace ImmoBundle\Form;

use PictureBundle\Entity\PieceJointeFactureClient;
use PictureBundle\Form\PictureType;
use PictureBundle\Form\PieceJointeFactureClientType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FactureClientType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type')
            ->add('numeroCommande')
            ->add('nomModel')
            ->add('montant')
            ->add('quantite')
            ->add('pieceJointe',PieceJointeFactureClientType::class)
            ->add('datePrevuDencaissement',null,[
                'widget' => 'single_text'
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ImmoBundle\Entity\FactureClient'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'immobundle_factureclient';
    }


}
