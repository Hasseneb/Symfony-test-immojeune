<?php

namespace ImmoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VehiculesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('immatriculation')
            ->add('model')
            ->add('assuranceDD',null,[
                'widget' => 'single_text'
            ])
            ->add('assuranceDF',null,[
                'widget' => 'single_text'
            ])
            ->add('visiteTechniqueDD',null,[
                'widget' => 'single_text'
            ])
            ->add('visiteTechniqueDF',null,[
                'widget' => 'single_text'
            ])
            ->add('permisBleuDD',null,[
                'widget' => 'single_text'
            ])
            ->add('permisBleuDF',null,[
                'widget' => 'single_text'
            ])
            ->add('taxeDD',null,[
                'widget' => 'single_text'
            ])
            ->add('taxeDF',null,[
                'widget' => 'single_text'
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ImmoBundle\Entity\Vehicules'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'immobundle_vehicules';
    }


}
