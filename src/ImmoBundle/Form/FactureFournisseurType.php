<?php

namespace ImmoBundle\Form;

use PictureBundle\Form\PictureType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FactureFournisseurType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('numeroCommande')->add('nomFournisseur')
            ->add('montant')->add('referenceArticle')->add('dateFacturation',null,[
                'widget' => 'single_text'
            ])->add('datePaiement',null,[
                'widget' => 'single_text'
            ])->add('photo',PictureType::class)
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ImmoBundle\Entity\FactureFournisseur'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'immobundle_facturefournisseur';
    }


}
