<?php
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 17/12/2017
 * Time: 01:17
 */

namespace ImmoBundle\Twig;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use ImmoBundle\Entity\Avance;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;

class avanceExtension extends \Twig_Extension
{
    public $controller;
    public function __construct(EntityManagerInterface $controller)
    {
        $this->controller = $controller;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('avance', array($this, 'avanceFilter')),
        );
    }

    public function avanceFilter($user){
        $date = date('d');
        if ($date <= 10 ){
            $date = date('c', mktime(0, 0, 0, date('m')-1, 1, date('Y')));
            $date2 = date('c', mktime(0, 0, 0, date('m'), 1, date('Y')));
        }else{
            $date = date('c', mktime(0, 0, 0, date('m'), 1, date('Y')));
            $date2 = date('c', mktime(0, 0, 0, date('m')+1, 1, date('Y')));
        }
        $avances = $this->controller->getRepository(Avance::class)->getAvancesByDate($user,$date,$date2);
        return  $avances;
    }
}