<?php
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 19/12/2017
 * Time: 16:28
 */

namespace ImmoBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StatController extends Controller
{
    public function homepageAction(){
        $date = date('c', mktime(0, 0, 0, date('m')-1, 1, date('Y')));
        $dateAvance = date('d');
        $thisMonth = date('c', mktime(0, 0, 0, date('m'), 1, date('Y')));
        $lastDayOfMonth = date('c', mktime(0, 0, 0, date('m'), 31, date('Y')));
        $salaire = $this->getDoctrine()->getRepository('ImmoBundle:Personnel')->getSalary();
        if ($dateAvance <= 10 ){
            $dateAvance = date('c', mktime(0, 0, 0, date('m')-1, 1, date('Y')));
            $date2 = date('c', mktime(0, 0, 0, date('m'), 10, date('Y')));
        }else{
            $dateAvance = date('c', mktime(0, 0, 0, date('m'), 10, date('Y')));
            $date2 = date('c', mktime(0, 0, 0, date('m')+1, 10, date('Y')));
        }
        $avances = $this->getDoctrine()->getRepository('ImmoBundle:Avance')->getAllAvancesByDate($dateAvance,$date2);
        $salariesNumber = $this->getDoctrine()->getRepository('ImmoBundle:Personnel')->getSalaryNumbers();
        $stockQuantity = $this->getDoctrine()->getRepository('ImmoBundle:Stock')->getStockQuantity();
        $salaireAPayer = $salaire - $avances;
        $vehicules = $this->getDoctrine()->getRepository('ImmoBundle:Vehicules')->findAll();
        $facturesFournisseurs = $this->getDoctrine()->getRepository('ImmoBundle:FactureFournisseur')->findBy(['status' => false]);
        $facturesClients = $this->getDoctrine()->getRepository('ImmoBundle:FactureClient')->findBy(['status' => false]);
        $notifications = $this->getDoctrine()->getRepository('ImmoBundle:Notification')->findBy(['idExterne' => null ]);
        $events = $this->getDoctrine()->getRepository('ImmoBundle:Notification')->getAllEvents(new \DateTime('now'));
        $stocks = $this->getDoctrine()->getRepository('ImmoBundle:Stock')->findAll();
        $fournitures = $this->getDoctrine()->getRepository('ImmoBundle:Fourniture')->findAll();
        $fournituresQuantity = $this->getDoctrine()->getRepository('ImmoBundle:Fourniture')->getFournitureQuantity();
        $totalEncaisse = $this->getDoctrine()->getRepository('ImmoBundle:FactureClient')->getTotalEncaisse($thisMonth,$lastDayOfMonth);
        $totalDepensee = $this->getDoctrine()->getRepository('ImmoBundle:FactureFournisseur')->getTotalEncaisse($thisMonth,$lastDayOfMonth);
        $gain = $totalEncaisse - $totalDepensee ;
        $stats = $this->monthlyStats();
        dump($stats[0]);
        return $this->render('default/index.html.twig',[
            'stocks' => $stocks,
            'stats' => $stats,
            'gain' => $gain,
            'totalEncaisse' => $totalEncaisse,
            'totalDepensee' => $totalDepensee,
            'fournitures' => $fournitures,
            'fournitureQuantity' => $fournituresQuantity,
            'events' => $events,
            'salaireApayer' => $salaireAPayer,
            'notifications' => $notifications,
            'salaire' => $salaire,
            'vehicules' => $vehicules,
            'facturesFournisseurs' => $facturesFournisseurs,
            'facturesClient' => $facturesClients,
            'salariesNumber' => $salariesNumber,
            'stock' => $stockQuantity,
            'avances' => $avances
        ]);
    }
    public function monthlyStats(){
        $depenses = [];
        $encaissemet = [];
        for ($i=1;$i<=12;$i++){
            $thisMonth = date('c', mktime(0, 0, 0, $i, 1, date('Y')));
            $lastDayOfMonth = date('c', mktime(0, 0, 0, $i, 31, date('Y')));
            $totalEncaisse = $this->getDoctrine()->getRepository('ImmoBundle:FactureClient')->getTotalEncaisse($thisMonth,$lastDayOfMonth);
            $totalDepensee = $this->getDoctrine()->getRepository('ImmoBundle:FactureFournisseur')->getTotalEncaisse($thisMonth,$lastDayOfMonth);
            array_push($depenses,$totalDepensee);
            array_push($encaissemet,$totalEncaisse);
        }
        return [$depenses,$encaissemet];

    }
}