<?php

namespace ImmoBundle\Controller;

use ImmoBundle\Entity\Bien;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;

/**
 * Bien controller.
 *
 */
class BienController extends Controller
{

    /**
     * Lists all bien entities of current user.
     *
     */
    public function mesBiensAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        if (!$user) {
            throw new NotFoundHttpException('User not found');
        }

        $biens = $em->getRepository('ImmoBundle:Bien')->findBy(['gestionnaire' => $user]);

        return $this->render(
            'bien/mesBiens.html.twig',
            array(
                'biens' => $biens,
            )
        );
    }

    /**
     * Deletes a bien entity from table
     *
     */
    public function deleteBienFromTableAction(Request $request, $id)
    {

        $user = $this->getUser();
        if (!$user) {
            throw new NotFoundHttpException('User Entity was not found');
        }
        $em = $this->getDoctrine()->getManager();
        $bien = $em->getRepository('ImmoBundle:Bien')->findOneBy(['id' => $id, 'gestionnaire' => $user]);

        if (!$bien) {
            throw new NotFoundHttpException('Bien Entity was  not found');
        }

        $em->remove($bien);
        $em->flush();

        $this->addFlash("success", "Le bien a été supprimé avec succés");

        return $this->redirectToRoute('user_index');
    }

    /**
     * Lists all bien entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $biens = $em->getRepository('ImmoBundle:Bien')->findAll();

        return $this->render(
            'bien/index.html.twig',
            array(
                'biens' => $biens,
            )
        );
    }

    /**
     * Creates a new bien entity.
     *
     */
    public function newAction(Request $request)
    {
        $bien = new Bien();
        $form = $this->createForm('ImmoBundle\Form\BienType', $bien);
        $user = $this->getUser();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $bien->setGestionnaire($user);
            $em->persist($bien);
            $em->flush();
            $this->addFlash("success", "Le bien a été ajouté avec succés");
            return $this->redirectToRoute('bien_show', array('id' => $bien->getId()));
        }

        return $this->render(
            'bien/new.html.twig',
            array(
                'bien' => $bien,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a bien entity.
     *
     */
    public function showAction(Bien $bien)
    {
        $deleteForm = $this->createDeleteForm($bien);

        return $this->render(
            'bien/show.html.twig',
            array(
                'bien' => $bien,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing bien entity.
     *
     */
    public function editAction(Request $request, Bien $bien)
    {
        $deleteForm = $this->createDeleteForm($bien);
        $editForm = $this->createForm('ImmoBundle\Form\BienType', $bien);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", "Le bien a été modifié avec succés");
            return $this->redirectToRoute('bien_edit', array('id' => $bien->getId()));
        }

        return $this->render(
            'bien/edit.html.twig',
            array(
                'bien' => $bien,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a bien entity.
     *
     */
    public function deleteAction(Request $request, Bien $bien)
    {
        $form = $this->createDeleteForm($bien);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($bien);
            $em->flush();
        }

        return $this->redirectToRoute('bien_index');
    }

    /**
     * Creates a form to delete a bien entity.
     *
     * @param Bien $bien The bien entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Bien $bien)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bien_delete', array('id' => $bien->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
