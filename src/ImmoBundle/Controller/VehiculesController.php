<?php

namespace ImmoBundle\Controller;

use ImmoBundle\Entity\Notification;
use ImmoBundle\Entity\Vehicules;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Vehicule controller.
 *
 */
class VehiculesController extends Controller
{
    /**
     * Lists all vehicule entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $vehicules = $em->getRepository('ImmoBundle:Vehicules')->findAll();

        return $this->render(
            'vehicules/index.html.twig',
            array(
                'vehicules' => $vehicules,
            )
        );
    }

    /**
     * Creates a new vehicule entity.
     *
     */
    public function newAction(Request $request)
    {
        $vehicule = new Vehicules();
        $form = $this->createForm('ImmoBundle\Form\VehiculesType', $vehicule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($vehicule);
            $em->flush();
            $notif1 = new Notification('Paiement Assurance', $vehicule->getAssuranceDF(), $vehicule->getId());
            $notif2 = new Notification('Visite Technique', $vehicule->getVisiteTechniqueDF(), $vehicule->getId());
            $notif3 = new Notification('Permis Bleu', $vehicule->getPermisBleuDF(), $vehicule->getId());
            $notif4 = new Notification('Taxe', $vehicule->getTaxeDF(), $vehicule->getId());
            $em->persist($notif1);
            $em->persist($notif2);
            $em->persist($notif3);
            $em->persist($notif4);
            $em->flush();
            $this->addFlash("add", "Le véhicule a été ajouté avec succés");

            return $this->redirectToRoute('vehicules_show', array('id' => $vehicule->getId()));
        }

        return $this->render(
            'vehicules/new.html.twig',
            array(
                'vehicule' => $vehicule,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a vehicule entity.
     *
     */
    public function showAction(Vehicules $vehicule)
    {
        $deleteForm = $this->createDeleteForm($vehicule);

        return $this->render(
            'vehicules/show.html.twig',
            array(
                'vehicule' => $vehicule,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing vehicule entity.
     *
     */
    public function editAction(Request $request, Vehicules $vehicule)
    {
        $deleteForm = $this->createDeleteForm($vehicule);
        $editForm = $this->createForm('ImmoBundle\Form\VehiculesType', $vehicule);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('vehicules_edit', array('id' => $vehicule->getId()));
        }

        return $this->render(
            'vehicules/edit.html.twig',
            array(
                'vehicule' => $vehicule,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a vehicule entity.
     *
     */
    public function deleteAction(Request $request, Vehicules $vehicule)
    {
        $form = $this->createDeleteForm($vehicule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($vehicule);
            $em->flush();
        }

        return $this->redirectToRoute('vehicules_index');
    }

    /**
     * Creates a form to delete a vehicule entity.
     *
     * @param Vehicules $vehicule The vehicule entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Vehicules $vehicule)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('vehicules_delete', array('id' => $vehicule->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function deletePersonnelFromTableAction(Request $request, $id)
    {

        $user = $this->getUser();
        if (!$user) {
            throw new NotFoundHttpException('User Entity was not found');
        }
        $em = $this->getDoctrine()->getManager();
        $bien = $em->getRepository('ImmoBundle:Vehicules')->findOneBy(['id' => $id]);

        if (!$bien) {
            throw new NotFoundHttpException('Vehicules Entity was  not found');
        }
        $notifications = $em->getRepository('ImmoBundle:Notification')->findBy(['idExterne' => $bien->getId()]);
        foreach ($notifications as $notification){
            $em->remove($notification);
        }
        $em->remove($bien);
        $em->flush();

        $this->addFlash("success", "Le véhicule a été supprimé avec succés");

        return $this->redirectToRoute('vehicules_index');
    }
}
