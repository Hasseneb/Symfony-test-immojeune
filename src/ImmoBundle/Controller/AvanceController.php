<?php

namespace ImmoBundle\Controller;

use ImmoBundle\Entity\Avance;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Avance controller.
 *
 */
class AvanceController extends Controller
{
    /**
     * Lists all avance entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $avances = $em->getRepository('ImmoBundle:Avance')->findAll();

        return $this->render('avance/index.html.twig', array(
            'avances' => $avances,
        ));
    }

    /**
     * Creates a new avance entity.
     *
     */
    public function newAction(Request $request)
    {
        $avance = new Avance();
        $form = $this->createForm('ImmoBundle\Form\AvanceType', $avance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($avance);
            $em->flush();
            $this->addFlash("add", "L'avance a été ajouté avec succés");
            return $this->redirectToRoute('avance_show', array('id' => $avance->getId()));
        }

        return $this->render('avance/new.html.twig', array(
            'avance' => $avance,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a avance entity.
     *
     */
    public function showAction(Avance $avance)
    {
        $deleteForm = $this->createDeleteForm($avance);

        return $this->render('avance/show.html.twig', array(
            'avance' => $avance,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing avance entity.
     *
     */
    public function editAction(Request $request, Avance $avance)
    {
        $deleteForm = $this->createDeleteForm($avance);
        $editForm = $this->createForm('ImmoBundle\Form\AvanceType', $avance);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("add", "L'avance a été modifié avec succés");
            return $this->redirectToRoute('avance_edit', array('id' => $avance->getId()));
        }

        return $this->render('avance/edit.html.twig', array(
            'avance' => $avance,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a avance entity.
     *
     */
    public function deleteAction(Request $request, Avance $avance)
    {
        $form = $this->createDeleteForm($avance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($avance);
            $em->flush();
        }

        return $this->redirectToRoute('avance_index');
    }

    /**
     * Creates a form to delete a avance entity.
     *
     * @param Avance $avance The avance entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Avance $avance)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('avance_delete', array('id' => $avance->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    public function deletePersonnelFromTableAction(Request $request, $id)
    {

        $user = $this->getUser();
        if (!$user) {
            throw new NotFoundHttpException('User Entity was not found');
        }
        $em = $this->getDoctrine()->getManager();
        $bien = $em->getRepository('ImmoBundle:Avance')->findOneBy(['id' => $id]);

        if (!$bien) {
            throw new NotFoundHttpException('Avance Entity was  not found');
        }

        $em->remove($bien);
        $em->flush();

        $this->addFlash("success", "L'avance a été supprimé avec succés");

        return $this->redirectToRoute('avance_index');
    }
}
