<?php

namespace ImmoBundle\Controller;

use ImmoBundle\Entity\Fourniture;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Fourniture controller.
 *
 */
class FournitureController extends Controller
{
    /**
     * Lists all fourniture entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $fournitures = $em->getRepository('ImmoBundle:Fourniture')->findAll();

        return $this->render('fourniture/index.html.twig', array(
            'fournitures' => $fournitures,
        ));
    }

    /**
     * Creates a new fourniture entity.
     *
     */
    public function newAction(Request $request)
    {
        $fourniture = new Fourniture();
        $form = $this->createForm('ImmoBundle\Form\FournitureType', $fourniture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fourniture);
            $em->flush();
            $this->addFlash("add", "La fourniture a été ajouté avec succés");
            return $this->redirectToRoute('fourniture_show', array('id' => $fourniture->getId()));
        }

        return $this->render('fourniture/new.html.twig', array(
            'fourniture' => $fourniture,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a fourniture entity.
     *
     */
    public function showAction(Fourniture $fourniture)
    {
        $deleteForm = $this->createDeleteForm($fourniture);

        return $this->render('fourniture/show.html.twig', array(
            'fourniture' => $fourniture,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing fourniture entity.
     *
     */
    public function editAction(Request $request, Fourniture $fourniture)
    {
        $deleteForm = $this->createDeleteForm($fourniture);
        $editForm = $this->createForm('ImmoBundle\Form\FournitureType', $fourniture);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("add", "La fourniture a été modifié avec succés");
            return $this->redirectToRoute('fourniture_edit', array('id' => $fourniture->getId()));
        }

        return $this->render('fourniture/edit.html.twig', array(
            'fourniture' => $fourniture,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a fourniture entity.
     *
     */
    public function deleteAction(Request $request, Fourniture $fourniture)
    {
        $form = $this->createDeleteForm($fourniture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fourniture);
            $em->flush();
        }

        return $this->redirectToRoute('fourniture_index');
    }

    /**
     * Creates a form to delete a fourniture entity.
     *
     * @param Fourniture $fourniture The fourniture entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Fourniture $fourniture)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('fourniture_delete', array('id' => $fourniture->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    public function deletePersonnelFromTableAction(Request $request, $id)
    {

        $user = $this->getUser();
        if (!$user) {
            throw new NotFoundHttpException('User Entity was not found');
        }
        $em = $this->getDoctrine()->getManager();
        $bien = $em->getRepository('ImmoBundle:Fourniture')->findOneBy(['id' => $id]);

        if (!$bien) {
            throw new NotFoundHttpException('Fourniture Entity was  not found');
        }

        $em->remove($bien);
        $em->flush();

        $this->addFlash("success", "La fourniture a été supprimé avec succés");

        return $this->redirectToRoute('fourniture_index');
    }
}
