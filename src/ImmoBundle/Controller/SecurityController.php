<?php
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 25/10/2017
 * Time: 19:26
 */

namespace ImmoBundle\Controller;


use ImmoBundle\Entity\Gestionnaire;
use ImmoBundle\Entity\User;
use ImmoBundle\Form\LoginType;
use ImmoBundle\Form\RegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends Controller
{

    public function loginAction(){
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        $form = $this->createForm(LoginType::class, [
            '_username' => $lastUsername,
        ]);

        return $this->render(
            '@Immo/Security/login.html.twig',
            array(
                'form' => $form->createView(),
                'error'         => $error,
            )
        );
    }

    public function registerAction(Request $request)
    {

        $gestionnaire = new User();
        $form = $this->createForm(RegistrationType::class, $gestionnaire);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($gestionnaire);
            $em->flush();



            return $this->redirectToRoute('security_login');
        }

        return $this->render(
            '@Immo/Security/registration.html.twig',
            array('form' => $form->createView())
        );
    }
    public function logoutAction(Request $request)
    {

    }

}