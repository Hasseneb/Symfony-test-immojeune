<?php

namespace ImmoBundle\Controller;

use ImmoBundle\Entity\Absence;
use ImmoBundle\Entity\Avance;
use ImmoBundle\Entity\Personnel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Personnel controller.
 *
 */
class PersonnelController extends Controller
{
    /**
     * Lists all personnel entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $personnels = $em->getRepository('ImmoBundle:Personnel')->findAll();

        return $this->render('personnel/index.html.twig', array(
            'personnels' => $personnels,
        ));
    }

    /**
     * Creates a new personnel entity.
     *
     */
    public function newAction(Request $request)
    {
        $personnel = new Personnel();
        $form = $this->createForm('ImmoBundle\Form\PersonnelType', $personnel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($personnel);
            $em->flush();
            $this->addFlash("add", "Le personnel a été ajouté avec succés");
            return $this->redirectToRoute('personnel_show', array('id' => $personnel->getId()));
        }

        return $this->render('personnel/new.html.twig', array(
            'personnel' => $personnel,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a personnel entity.
     *
     */
    public function showAction(Personnel $personnel)
    {
        $deleteForm = $this->createDeleteForm($personnel);
        $date = date('d');
        if ($date <= 10 ){
            $date = date('c', mktime(0, 0, 0, date('m')-1, 1, date('Y')));
            $date2 = date('c', mktime(0, 0, 0, date('m'), 10, date('Y')));
        }else{
            $date = date('c', mktime(0, 0, 0, date('m'), 10, date('Y')));
            $date2 = date('c', mktime(0, 0, 0, date('m')+1, 10, date('Y')));
        }
        $avances = $this->getDoctrine()->getRepository(Avance::class)->getAvancesObjectsByDate($personnel,$date,$date2);
        $absences = $this->getDoctrine()->getRepository(Absence::class)->getAbsencesObjectsByDate($personnel,$date,$date2);

        return $this->render('personnel/show.html.twig', array(
            'personnel' => $personnel,
            'avances' => $avances,
            'absences' => $absences,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing personnel entity.
     *
     */
    public function editAction(Request $request, Personnel $personnel)
    {
        $deleteForm = $this->createDeleteForm($personnel);
        $editForm = $this->createForm('ImmoBundle\Form\PersonnelType', $personnel);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("add", "Le personnel a été modifié avec succés");
            return $this->redirectToRoute('personnel_edit', array('id' => $personnel->getId()));
        }

        return $this->render('personnel/edit.html.twig', array(
            'personnel' => $personnel,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a personnel entity.
     *
     */
    public function deleteAction(Request $request, Personnel $personnel)
    {
        $form = $this->createDeleteForm($personnel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($personnel);
            $em->flush();
        }

        return $this->redirectToRoute('personnel_index');
    }

    /**
     * Creates a form to delete a personnel entity.
     *
     * @param Personnel $personnel The personnel entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Personnel $personnel)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('personnel_delete', array('id' => $personnel->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    public function deletePersonnelFromTableAction(Request $request, $id)
    {

        $user = $this->getUser();
        if (!$user) {
            throw new NotFoundHttpException('User Entity was not found');
        }
        $em = $this->getDoctrine()->getManager();
        $bien = $em->getRepository('ImmoBundle:Personnel')->findOneBy(['id' => $id]);

        if (!$bien) {
            throw new NotFoundHttpException('Personnel Entity was  not found');
        }

        $em->remove($bien);
        $em->flush();

        $this->addFlash("success", "Le personnel a été supprimé avec succés");

        return $this->redirectToRoute('personnel_index');
    }
}
