<?php

namespace ImmoBundle\Controller;

use ImmoBundle\Entity\Absence;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Absence controller.
 *
 */
class AbsenceController extends Controller
{
    /**
     * Lists all absence entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $absences = $em->getRepository('ImmoBundle:Absence')->findAll();

        return $this->render(
            'absence/index.html.twig',
            array(
                'absences' => $absences,
            )
        );
    }

    /**
     * Creates a new absence entity.
     *
     */
    public function newAction(Request $request)
    {
        $form = $this->createForm('ImmoBundle\Form\AbsenceType', null);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $form->getData();
            foreach ($data['personnel'] as $personnel) {
                $absence = new Absence();
                $absence->setDateAbsence($data['dateAbsence']);
                $absence->setPersonnel($personnel);
                $em->persist($absence);
            }
            $em->flush();
            $this->addFlash("add", "L'absence a été ajouté avec succés");

            return $this->redirectToRoute('absence_index');
        }

        return $this->render(
            'absence/new.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a absence entity.
     *
     */
    public function showAction(Absence $absence)
    {
        $deleteForm = $this->createDeleteForm($absence);

        return $this->render(
            'absence/show.html.twig',
            array(
                'absence' => $absence,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing absence entity.
     *
     */
    public function editAction(Request $request, Absence $absence)
    {
        $deleteForm = $this->createDeleteForm($absence);
        $editForm = $this->createForm('ImmoBundle\Form\AbsenceType', $absence);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("add", "L'absence a été modifié avec succés");

            return $this->redirectToRoute('absence_edit', array('id' => $absence->getId()));
        }

        return $this->render(
            'absence/edit.html.twig',
            array(
                'absence' => $absence,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a absence entity.
     *
     */
    public function deleteAction(Request $request, Absence $absence)
    {
        $form = $this->createDeleteForm($absence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($absence);
            $em->flush();
        }

        return $this->redirectToRoute('absence_index');
    }

    /**
     * Creates a form to delete a absence entity.
     *
     * @param Absence $absence The absence entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Absence $absence)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('absence_delete', array('id' => $absence->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function deleteAbsenceFromTableAction(Request $request, $id)
    {

        $user = $this->getUser();
        if (!$user) {
            throw new NotFoundHttpException('User Entity was not found');
        }
        $em = $this->getDoctrine()->getManager();
        $bien = $em->getRepository('ImmoBundle:Absence')->findOneBy(['id' => $id]);

        if (!$bien) {
            throw new NotFoundHttpException('Absence Entity was  not found');
        }

        $em->remove($bien);
        $em->flush();

        $this->addFlash("success", "L'absence a été supprimé avec succés");

        return $this->redirectToRoute('absence_index');
    }
}
