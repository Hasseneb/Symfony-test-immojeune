<?php

namespace ImmoBundle\Controller;

use ImmoBundle\Entity\FactureFournisseur;
use ImmoBundle\Entity\Notification;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Facturefournisseur controller.
 *
 */
class FactureFournisseurController extends Controller
{
    /**
     * Lists all factureFournisseur entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $factureFournisseurs = $em->getRepository('ImmoBundle:FactureFournisseur')->findAll();

        return $this->render(
            'facturefournisseur/index.html.twig',
            array(
                'factureFournisseurs' => $factureFournisseurs,
            )
        );
    }

    /**
     * Creates a new factureFournisseur entity.
     *
     */
    public function newAction(Request $request)
    {
        $factureFournisseur = new Facturefournisseur();
        $form = $this->createForm('ImmoBundle\Form\FactureFournisseurType', $factureFournisseur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($factureFournisseur);
            $em->flush();
            $notif1 = new Notification(
                'Paiement Facture Fournisseur',
                $factureFournisseur->getDatePaiement(),
                $factureFournisseur->getId()
            );
            $em->persist($notif1);
            $em->flush();
            $this->addFlash("add", "La facture fournisseur a été ajouté avec succés");

            return $this->redirectToRoute('facturefournisseur_show', array('id' => $factureFournisseur->getId()));
        }

        return $this->render(
            'facturefournisseur/new.html.twig',
            array(
                'factureFournisseur' => $factureFournisseur,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a factureFournisseur entity.
     *
     */
    public function showAction(FactureFournisseur $factureFournisseur)
    {
        $deleteForm = $this->createDeleteForm($factureFournisseur);

        return $this->render(
            'facturefournisseur/show.html.twig',
            array(
                'factureFournisseur' => $factureFournisseur,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing factureFournisseur entity.
     *
     */
    public function editAction(Request $request, FactureFournisseur $factureFournisseur)
    {
        $deleteForm = $this->createDeleteForm($factureFournisseur);
        $editForm = $this->createForm('ImmoBundle\Form\FactureFournisseurEditType', $factureFournisseur);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash("add", "La facture fournisseur a été modifié avec succés");
            return $this->redirectToRoute('facturefournisseur_edit', array('id' => $factureFournisseur->getId()));
        }

        return $this->render(
            'facturefournisseur/edit.html.twig',
            array(
                'factureFournisseur' => $factureFournisseur,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a factureFournisseur entity.
     *
     */
    public function deleteAction(Request $request, FactureFournisseur $factureFournisseur)
    {
        $form = $this->createDeleteForm($factureFournisseur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($factureFournisseur);
            $em->flush();
        }

        return $this->redirectToRoute('facturefournisseur_index');
    }

    /**
     * Creates a form to delete a factureFournisseur entity.
     *
     * @param FactureFournisseur $factureFournisseur The factureFournisseur entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(FactureFournisseur $factureFournisseur)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('facturefournisseur_delete', array('id' => $factureFournisseur->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function deletePersonnelFromTableAction(Request $request, $id)
    {

        $user = $this->getUser();
        if (!$user) {
            throw new NotFoundHttpException('User Entity was not found');
        }
        $em = $this->getDoctrine()->getManager();
        $bien = $em->getRepository('ImmoBundle:FactureFournisseur')->findOneBy(['id' => $id]);

        if (!$bien) {
            throw new NotFoundHttpException('Personnel Entity was  not found');
        }

        $em->remove($bien);
        $em->flush();

        $this->addFlash("success", "La facture fournisseur a été supprimé avec succés");

        return $this->redirectToRoute('facturefournisseur_index');
    }
    public function changeStatusAction($id){
        $user = $this->getUser();
        if (!$user) {
            throw new NotFoundHttpException('User Entity was not found');
        }
        $em = $this->getDoctrine()->getManager();
        $bien = $em->getRepository('ImmoBundle:FactureFournisseur')->findOneBy(['id' => $id]);

        if (!$bien) {
            throw new NotFoundHttpException('Facture fournisseur Entity was  not found');
        }

        $status = $bien->getStatus();

        if ($status == false ) {
            $bien->setStatus(true);
        }else{
            $bien->setStatus(false);
        }
        $em->flush();
        $this->addFlash("success", "Le statut de la facture a été modifié avec succés");
        return $this->redirectToRoute('facturefournisseur_index');

    }
}
