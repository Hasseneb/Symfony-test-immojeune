<?php

namespace ImmoBundle\Controller;

use ImmoBundle\Entity\FactureClient;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Factureclient controller.
 *
 */
class FactureClientController extends Controller
{
    /**
     * Lists all factureClient entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $factureClients = $em->getRepository('ImmoBundle:FactureClient')->findAll();

        return $this->render('factureclient/index.html.twig', array(
            'factureClients' => $factureClients,
        ));
    }

    /**
     * Creates a new factureClient entity.
     *
     */
    public function newAction(Request $request)
    {
        $factureClient = new Factureclient();
        $form = $this->createForm('ImmoBundle\Form\FactureClientType', $factureClient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($factureClient);
            $em->flush();
            $this->addFlash("add", "La facture a été ajouté avec succés");
            return $this->redirectToRoute('factureclient_show', array('id' => $factureClient->getId()));
        }

        return $this->render('factureclient/new.html.twig', array(
            'factureClient' => $factureClient,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a factureClient entity.
     *
     */
    public function showAction(FactureClient $factureClient)
    {
        $deleteForm = $this->createDeleteForm($factureClient);

        return $this->render('factureclient/show.html.twig', array(
            'factureClient' => $factureClient,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * @param Request $request
     * @param FactureClient $factureClient
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, FactureClient $factureClient)
    {
        $deleteForm = $this->createDeleteForm($factureClient);
        $editForm = $this->createForm('ImmoBundle\Form\FactureClientEditType', $factureClient);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            if ($editForm->get('pieceJointe') !== null){
                $piece = $factureClient->getPieceJointe()->setFactureClient(null);
                if ($factureClient->getPieceJointe() !== null){
                    $this->getDoctrine()->getManager()->remove($piece);
                    $this->getDoctrine()->getManager()->flush();
                }
                $factureClient->setPieceJointe($editForm->get('pieceJointe')->getData());


            }

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("add", "La facture client a été modifié avec succés");
            return $this->redirectToRoute('factureclient_edit', array('id' => $factureClient->getId()));
        }

        return $this->render('factureclient/edit.html.twig', array(
            'factureClient' => $factureClient,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a factureClient entity.
     *
     */
    public function deleteAction(Request $request, FactureClient $factureClient)
    {
        $form = $this->createDeleteForm($factureClient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($factureClient);
            $em->flush();
        }

        return $this->redirectToRoute('factureclient_index');
    }

    /**
     * Creates a form to delete a factureClient entity.
     *
     * @param FactureClient $factureClient The factureClient entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(FactureClient $factureClient)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('factureclient_delete', array('id' => $factureClient->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    public function deletePersonnelFromTableAction(Request $request, $id)
    {

        $user = $this->getUser();
        if (!$user) {
            throw new NotFoundHttpException('User Entity was not found');
        }
        $em = $this->getDoctrine()->getManager();
        $bien = $em->getRepository('ImmoBundle:FactureClient')->findOneBy(['id' => $id]);

        if (!$bien) {
            throw new NotFoundHttpException('Facture client Entity was  not found');
        }

        $em->remove($bien);
        $em->flush();

        $this->addFlash("success", "La facture client a été supprimé avec succés");

        return $this->redirectToRoute('factureclient_index');
    }

    public function changeStatusAction($id){
        $user = $this->getUser();
        if (!$user) {
            throw new NotFoundHttpException('User Entity was not found');
        }
        $em = $this->getDoctrine()->getManager();
        $bien = $em->getRepository('ImmoBundle:FactureClient')->findOneBy(['id' => $id]);

        if (!$bien) {
            throw new NotFoundHttpException('Facture client Entity was  not found');
        }

        $status = $bien->getStatus();

        if ($status == false ) {
            $bien->setStatus(true);
        }else{
            $bien->setStatus(false);
        }
        $em->flush();
        $this->addFlash("success", "Le statut de la facture a été modifié avec succés");
        return $this->redirectToRoute('factureclient_index');

    }

}
