<?php

namespace ImmoBundle\Controller;

use ImmoBundle\Entity\Locataire;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Locataire controller.
 *
 */
class LocataireController extends Controller
{

    /**
     * Lists all locataire entities of current user.
     *
     */
    public function mesLocatairesAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        if (!$user) {
            throw new NotFoundHttpException('User not found');
        }
        $biens = $em->getRepository('ImmoBundle:Bien')->findBy(['gestionnaire' => $user]);

        $locataires = $em->getRepository('ImmoBundle:Locataire')->findBy(['bien' => $biens]);

        return $this->render(
            ':locataire:mesLocataire.html.twig',
            array(
                'locataires' => $locataires,
            )
        );
    }
    /**
     * Deletes a locataire entity from table
     *
     */
    public function deleteBienFromTableAction(Request $request, $id)
    {

        $user = $this->getUser();
        if (!$user) {
            throw new NotFoundHttpException('User Entity was not found');
        }
        $em = $this->getDoctrine()->getManager();

        $biens = $em->getRepository('ImmoBundle:Bien')->findBy(['gestionnaire' => $user]);

        $locataire = $em->getRepository('ImmoBundle:Locataire')->findOneBy(['id' => $id, 'bien' => $biens]);

        if (!$locataire) {
            throw new NotFoundHttpException('Locataire Entity was  not found');
        }

        $em->remove($locataire);
        $em->flush();

        $this->addFlash("success", "Le locataire a été supprimé avec succés");

        return $this->redirectToRoute('mes_locataires');
    }

    /**
     * Lists all locataire entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $locataires = $em->getRepository('ImmoBundle:Locataire')->findAll();

        return $this->render('locataire/index.html.twig', array(
            'locataires' => $locataires,
        ));
    }

    /**
     * Creates a new locataire entity.
     *
     */
    public function newAction(Request $request)
    {
        $locataire = new Locataire();
        $form = $this->createForm('ImmoBundle\Form\LocataireAvecBienType', $locataire ,['user' => $this->getUser()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($locataire);
            $em->flush();

            return $this->redirectToRoute('locataire_show', array('id' => $locataire->getId()));
        }

        return $this->render('locataire/new.html.twig', array(
            'locataire' => $locataire,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a locataire entity.
     *
     */
    public function showAction(Locataire $locataire)
    {
        $deleteForm = $this->createDeleteForm($locataire);

        return $this->render('locataire/show.html.twig', array(
            'locataire' => $locataire,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing locataire entity.
     *
     */
    public function editAction(Request $request, Locataire $locataire)
    {
        $deleteForm = $this->createDeleteForm($locataire);
        $editForm = $this->createForm('ImmoBundle\Form\LocataireType', $locataire);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('locataire_edit', array('id' => $locataire->getId()));
        }

        return $this->render('locataire/edit.html.twig', array(
            'locataire' => $locataire,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a locataire entity.
     *
     */
    public function deleteAction(Request $request, Locataire $locataire)
    {
        $form = $this->createDeleteForm($locataire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($locataire);
            $em->flush();
        }

        return $this->redirectToRoute('locataire_index');
    }

    /**
     * Creates a form to delete a locataire entity.
     *
     * @param Locataire $locataire The locataire entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Locataire $locataire)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('locataire_delete', array('id' => $locataire->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
