<?php
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 26/12/2017
 * Time: 03:42
 */

namespace ImmoBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FrontController extends Controller
{
    public function homepageAction(){
        return $this->render('front/homepage.html.twig');
    }

}