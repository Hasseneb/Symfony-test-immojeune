<?php

namespace ImmoBundle\Entity;

use PictureBundle\Entity\Picture;
use PictureBundle\Entity\PieceJointeFactureClient;

/**
 * FactureClient
 */
class FactureClient
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $numeroCommande;

    /**
     * @var string
     */
    private $quantite;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return FactureClient
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set numeroCommande
     *
     * @param string $numeroCommande
     *
     * @return FactureClient
     */
    public function setNumeroCommande($numeroCommande)
    {
        $this->numeroCommande = $numeroCommande;

        return $this;
    }

    /**
     * Get numeroCommande
     *
     * @return string
     */
    public function getNumeroCommande()
    {
        return $this->numeroCommande;
    }

    /**
     * Set quantite
     *
     * @param string $quantite
     *
     * @return FactureClient
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return string
     */
    public function getQuantite()
    {
        return $this->quantite;
    }
    /**
     * @var boolean
     */
    private $status = false;


    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return FactureClient
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * @var string
     */
    private $montant;


    /**
     * Set montant
     *
     * @param string $montant
     *
     * @return FactureClient
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return string
     */
    public function getMontant()
    {
        return $this->montant;
    }
    /**
     * @var \DateTime
     */
    private $datePrevuDencaissement;


    /**
     * Set datePrevuDencaissement
     *
     * @param \DateTime $datePrevuDencaissement
     *
     * @return FactureClient
     */
    public function setDatePrevuDencaissement($datePrevuDencaissement)
    {
        $this->datePrevuDencaissement = $datePrevuDencaissement;

        return $this;
    }

    /**
     * Get datePrevuDencaissement
     *
     * @return \DateTime
     */
    public function getDatePrevuDencaissement()
    {
        return $this->datePrevuDencaissement;
    }
   
    /**
     * @var string
     */
    private $nomModel;


    /**
     * Set nomModel
     *
     * @param string $nomModel
     *
     * @return FactureClient
     */
    public function setNomModel($nomModel)
    {
        $this->nomModel = $nomModel;

        return $this;
    }

    /**
     * Get nomModel
     *
     * @return string
     */
    public function getNomModel()
    {
        return $this->nomModel;
    }
    /**
     * @var PieceJointeFactureClient
     */
    private $pieceJointe;


    /**
     * @param PieceJointeFactureClient|null $pieceJointe
     * @return $this
     */
    public function setPieceJointe(PieceJointeFactureClient $pieceJointe = null)
    {
        $file = $pieceJointe->file;

        if (isset($file)) {
            $photo = new PieceJointeFactureClient();
            $photo->upload();
            $photo->setFactureClient($this);
            $photo->setFile($file);
            $this->pieceJointe = $photo;
        }

        return $this;
    }

    /**
     * @return PieceJointeFactureClient
     */
    public function getPieceJointe()
    {
        return $this->pieceJointe;
    }
}
