<?php

namespace ImmoBundle\Entity;

/**
 * FactureFournisseur
 */
class FactureFournisseur
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $numeroCommande;

    /**
     * @var string
     */
    private $nomFournisseur;

    /**
     * @var string
     */
    private $montant;

    /**
     * @var string
     */
    private $referenceArticle;

    /**
     * @var string
     */
    private $dateFacturation;

    /**
     * @var string
     */
    private $datePaiement;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numeroCommande
     *
     * @param string $numeroCommande
     *
     * @return FactureFournisseur
     */
    public function setNumeroCommande($numeroCommande)
    {
        $this->numeroCommande = $numeroCommande;

        return $this;
    }

    /**
     * Get numeroCommande
     *
     * @return string
     */
    public function getNumeroCommande()
    {
        return $this->numeroCommande;
    }

    /**
     * Set nomFournisseur
     *
     * @param string $nomFournisseur
     *
     * @return FactureFournisseur
     */
    public function setNomFournisseur($nomFournisseur)
    {
        $this->nomFournisseur = $nomFournisseur;

        return $this;
    }

    /**
     * Get nomFournisseur
     *
     * @return string
     */
    public function getNomFournisseur()
    {
        return $this->nomFournisseur;
    }

    /**
     * Set montant
     *
     * @param string $montant
     *
     * @return FactureFournisseur
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return string
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set referenceArticle
     *
     * @param string $referenceArticle
     *
     * @return FactureFournisseur
     */
    public function setReferenceArticle($referenceArticle)
    {
        $this->referenceArticle = $referenceArticle;

        return $this;
    }

    /**
     * Get referenceArticle
     *
     * @return string
     */
    public function getReferenceArticle()
    {
        return $this->referenceArticle;
    }

    /**
     * Set dateFacturation
     *
     * @param string $dateFacturation
     *
     * @return FactureFournisseur
     */
    public function setDateFacturation($dateFacturation)
    {
        $this->dateFacturation = $dateFacturation;

        return $this;
    }

    /**
     * Get dateFacturation
     *
     * @return string
     */
    public function getDateFacturation()
    {
        return $this->dateFacturation;
    }

    /**
     * Set datePaiement
     *
     * @param string $datePaiement
     *
     * @return FactureFournisseur
     */
    public function setDatePaiement($datePaiement)
    {
        $this->datePaiement = $datePaiement;

        return $this;
    }

    /**
     * Get datePaiement
     *
     * @return string
     */
    public function getDatePaiement()
    {
        return $this->datePaiement;
    }
    /**
     * @var boolean
     */
    private $status = false;


    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return FactureFournisseur
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }
}
