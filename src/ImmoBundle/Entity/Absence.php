<?php

namespace ImmoBundle\Entity;

/**
 * Absence
 */
class Absence
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateAbsence;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateAbsence
     *
     * @param \DateTime $dateAbsence
     *
     * @return Absence
     */
    public function setDateAbsence($dateAbsence)
    {
        $this->dateAbsence = $dateAbsence;

        return $this;
    }

    /**
     * Get dateAbsence
     *
     * @return \DateTime
     */
    public function getDateAbsence()
    {
        return $this->dateAbsence;
    }
    /**
     * @var \ImmoBundle\Entity\Personnel
     */
    private $personnel;


    /**
     * Set personnel
     *
     * @param \ImmoBundle\Entity\Personnel $personnel
     *
     * @return Absence
     */
    public function setPersonnel(\ImmoBundle\Entity\Personnel $personnel = null)
    {
        $this->personnel = $personnel;

        return $this;
    }

    /**
     * Get personnel
     *
     * @return \ImmoBundle\Entity\Personnel
     */
    public function getPersonnel()
    {
        return $this->personnel;
    }
}
