<?php

namespace ImmoBundle\Entity;

/**
 * Avance
 */
class Avance
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $personnel;

    /**
     * @var string
     */
    private $somme;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set personnel
     *
     * @param string $personnel
     *
     * @return Avance
     */
    public function setPersonnel($personnel)
    {
        $this->personnel = $personnel;

        return $this;
    }

    /**
     * Get personnel
     *
     * @return string
     */
    public function getPersonnel()
    {
        return $this->personnel;
    }

    /**
     * Set somme
     *
     * @param string $somme
     *
     * @return Avance
     */
    public function setSomme($somme)
    {
        $this->somme = $somme;

        return $this;
    }

    /**
     * Get somme
     *
     * @return string
     */
    public function getSomme()
    {
        return $this->somme;
    }
    /**
     * @var \DateTime
     */
    private $dateAttribution;


    /**
     * Set dateAttribution
     *
     * @param \DateTime $dateAttribution
     *
     * @return Avance
     */
    public function setDateAttribution($dateAttribution)
    {
        $this->dateAttribution = $dateAttribution;

        return $this;
    }

    /**
     * Get dateAttribution
     *
     * @return \DateTime
     */
    public function getDateAttribution()
    {
        return $this->dateAttribution;
    }
}
