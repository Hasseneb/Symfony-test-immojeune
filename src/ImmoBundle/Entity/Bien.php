<?php

namespace ImmoBundle\Entity;

/**
 * Bien
 */
class Bien
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $titre;

    /**
     * @var string
     */
    private $prixHorsCharges;

    /**
     * @var string
     */
    private $surface;

    /**
     * @var string
     */
    private $localisation;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Bien
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set prixHorsCharges
     *
     * @param string $prixHorsCharges
     *
     * @return Bien
     */
    public function setPrixHorsCharges($prixHorsCharges)
    {
        $this->prixHorsCharges = $prixHorsCharges;

        return $this;
    }

    /**
     * Get prixHorsCharges
     *
     * @return string
     */
    public function getPrixHorsCharges()
    {
        return $this->prixHorsCharges;
    }

    /**
     * Set surface
     *
     * @param string $surface
     *
     * @return Bien
     */
    public function setSurface($surface)
    {
        $this->surface = $surface;

        return $this;
    }

    /**
     * Get surface
     *
     * @return string
     */
    public function getSurface()
    {
        return $this->surface;
    }

    /**
     * Set localisation
     *
     * @param string $localisation
     *
     * @return Bien
     */
    public function setLocalisation($localisation)
    {
        $this->localisation = $localisation;

        return $this;
    }

    /**
     * Get localisation
     *
     * @return string
     */
    public function getLocalisation()
    {
        return $this->localisation;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $locataire;

    /**
     * @var \ImmoBundle\Entity\Gestionnaire
     */
    private $gestionnaire;

    /**
     * @return Gestionnaire
     */
    public function getGestionnaire()
    {
        return $this->gestionnaire;
    }

    /**
     * @param Gestionnaire $gestionnaire
     */
    public function setGestionnaire($gestionnaire)
    {
        $this->gestionnaire = $gestionnaire;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->locataire = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add locataire
     *
     * @param \ImmoBundle\Entity\Locataire $locataire
     *
     * @return Bien
     */
    public function addLocataire(\ImmoBundle\Entity\Locataire $locataire)
    {
        $this->locataire->add($locataire);
        $locataire->setBien($this);

        return $this;
    }

    /**
     * Remove locataire
     *
     * @param \ImmoBundle\Entity\Locataire $locataire
     */
    public function removeLocataire(\ImmoBundle\Entity\Locataire $locataire)
    {
        $this->locataire->removeElement($locataire);
    }

    /**
     * Get locataire
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocataire()
    {
        return $this->locataire;
    }
    /**
     * @var string
     */
    private $prixDesCharges;


    /**
     * Set prixDesCharges
     *
     * @param string $prixDesCharges
     *
     * @return Bien
     */
    public function setPrixDesCharges($prixDesCharges)
    {
        $this->prixDesCharges = $prixDesCharges;

        return $this;
    }

    /**
     * Get prixDesCharges
     *
     * @return string
     */
    public function getPrixDesCharges()
    {
        return $this->prixDesCharges;
    }

    function __toString()
    {
        return  $this->titre;
    }


}
