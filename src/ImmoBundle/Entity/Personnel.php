<?php

namespace ImmoBundle\Entity;

use PictureBundle\Entity\DossierMedical;
use PictureBundle\Entity\Picture;

/**
 * Personnel
 */
class Personnel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $fonction;

    /**
     * @var string
     */
    private $grade;

    /**
     * @var string
     */
    private $salaire;

    /**
     * @var string
     */
    private $avance;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $cin;

    /**
     * @var string
     */
    private $numero;

    /**
     * @var string
     */
    private $numeroCnss;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fonction
     *
     * @param string $fonction
     *
     * @return Personnel
     */
    public function setFonction($fonction)
    {
        $this->fonction = $fonction;

        return $this;
    }

    /**
     * Get fonction
     *
     * @return string
     */
    public function getFonction()
    {
        return $this->fonction;
    }

    /**
     * Set grade
     *
     * @param string $grade
     *
     * @return Personnel
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return string
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set salaire
     *
     * @param string $salaire
     *
     * @return Personnel
     */
    public function setSalaire($salaire)
    {
        $this->salaire = $salaire;

        return $this;
    }

    /**
     * Get salaire
     *
     * @return string
     */
    public function getSalaire()
    {
        return $this->salaire;
    }

    /**
     * Set avance
     *
     * @param string $avance
     *
     * @return Personnel
     */
    public function setAvance($avance)
    {
        $this->avance = $avance;

        return $this;
    }

    /**
     * Get avance
     *
     * @return string
     */
    public function getAvance()
    {
        return $this->avance;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Personnel
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set cin
     *
     * @param string $cin
     *
     * @return Personnel
     */
    public function setCin($cin)
    {
        $this->cin = $cin;

        return $this;
    }

    /**
     * Get cin
     *
     * @return string
     */
    public function getCin()
    {
        return $this->cin;
    }

    /**
     * Set numero
     *
     * @param string $numero
     *
     * @return Personnel
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set numeroCnss
     *
     * @param string $numeroCnss
     *
     * @return Personnel
     */
    public function setNumeroCnss($numeroCnss)
    {
        $this->numeroCnss = $numeroCnss;

        return $this;
    }

    /**
     * Get numeroCnss
     *
     * @return string
     */
    public function getNumeroCnss()
    {
        return $this->numeroCnss;
    }

    /**
     * @var string
     */
    private $prenom;


    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Personnel
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    function __toString()
    {
        return $this->prenom . ' ' .$this->nom;
    }




    /**
     * Constructor
     */
    public function __construct()
    {
        $this->avance = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add avance
     *
     * @param \ImmoBundle\Entity\Avance $avance
     *
     * @return Personnel
     */
    public function addAvance(\ImmoBundle\Entity\Avance $avance)
    {
        $this->avance[] = $avance;

        return $this;
    }

    /**
     * Remove avance
     *
     * @param \ImmoBundle\Entity\Avance $avance
     */
    public function removeAvance(\ImmoBundle\Entity\Avance $avance)
    {
        $this->avance->removeElement($avance);
    }
    /**
     * @var \PictureBundle\Entity\Picture
     */
    private $photo;


    /**
     * Set photo
     *
     * @param  Picture $photo
     *
     * @return Personnel
     */
    public function setPhoto($photo = null)
    {

        $file = $photo->file;
        if (isset($file)) {
            $photo = new Picture();
            $photo->upload();
            $photo->setPersonnel($this);
            $photo->setFile($file);
            $this->photo = $photo;
        }

        return $this;
    }

    /**
     * Get photo
     *
     * @return \PictureBundle\Entity\Picture
     */
    public function getPhoto()
    {
        return $this->photo;
    }
    /**
     * @var \PictureBundle\Entity\DossierMedical
     */
    private $dossierMedical;


    /**
     * Set dossierMedical
     *
     * @param Picture $dossierMedical
     *
     * @return Personnel
     */
    public function setDossierMedical($dossierMedical = null)
    {
        $file = $dossierMedical->file;
        if (isset($file)){
            $photo = new DossierMedical();
            $photo->upload();
            $photo->setPersonnel($this);
            $photo->setFile($file);
            $this->dossierMedical = $photo;
        }

        return $this;
    }

    /**
     * Get dossierMedical
     *
     * @return \PictureBundle\Entity\DossierMedical
     */
    public function getDossierMedical()
    {
        return $this->dossierMedical;
    }
    /**
     * @var \DateTime
     */
    private $dateEmbauche;

    /**
     * @var \DateTime
     */
    private $dateFinContrat;

    /**
     * @var string
     */
    private $discipline;


    /**
     * Set dateEmbauche
     *
     * @param \DateTime $dateEmbauche
     *
     * @return Personnel
     */
    public function setDateEmbauche($dateEmbauche)
    {
        $this->dateEmbauche = $dateEmbauche;

        return $this;
    }

    /**
     * Get dateEmbauche
     *
     * @return \DateTime
     */
    public function getDateEmbauche()
    {
        return $this->dateEmbauche;
    }

    /**
     * Set dateFinContrat
     *
     * @param \DateTime $dateFinContrat
     *
     * @return Personnel
     */
    public function setDateFinContrat($dateFinContrat)
    {
        $this->dateFinContrat = $dateFinContrat;

        return $this;
    }

    /**
     * Get dateFinContrat
     *
     * @return \DateTime
     */
    public function getDateFinContrat()
    {
        return $this->dateFinContrat;
    }

    /**
     * Set discipline
     *
     * @param string $discipline
     *
     * @return Personnel
     */
    public function setDiscipline($discipline)
    {
        $this->discipline = $discipline;

        return $this;
    }

    /**
     * Get discipline
     *
     * @return string
     */
    public function getDiscipline()
    {
        return $this->discipline;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $absence;


    /**
     * Add absence
     *
     * @param \ImmoBundle\Entity\Absence $absence
     *
     * @return Personnel
     */
    public function addAbsence(\ImmoBundle\Entity\Absence $absence)
    {
        $this->absence[] = $absence;

        return $this;
    }

    /**
     * Remove absence
     *
     * @param \ImmoBundle\Entity\Absence $absence
     */
    public function removeAbsence(\ImmoBundle\Entity\Absence $absence)
    {
        $this->absence->removeElement($absence);
    }

    /**
     * Get absence
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAbsence()
    {
        return $this->absence;
    }

    /**
     * Add fonction
     *
     * @param \ImmoBundle\Entity\Fonction $fonction
     *
     * @return Personnel
     */
    public function addFonction(\ImmoBundle\Entity\Fonction $fonction)
    {
        $this->fonction[] = $fonction;

        return $this;
    }

    /**
     * Remove fonction
     *
     * @param \ImmoBundle\Entity\Fonction $fonction
     */
    public function removeFonction(\ImmoBundle\Entity\Fonction $fonction)
    {
        $this->fonction->removeElement($fonction);
    }
}
