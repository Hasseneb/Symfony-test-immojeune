<?php

namespace ImmoBundle\Entity;

/**
 * Fonction
 */
class Fonction
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $hourPrice;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Fonction
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }



    /**
     * Set hourPrice
     *
     * @param string $hourPrice
     *
     * @return Fonction
     */
    public function setHourPrice($hourPrice)
    {
        $this->hourPrice = $hourPrice;

        return $this;
    }

    /**
     * Get hourPrice
     *
     * @return string
     */
    public function getHourPrice()
    {
        return $this->hourPrice;
    }
    /**
     * @var \ImmoBundle\Entity\Personnel
     */
    private $personnel;


    /**
     * Set personnel
     *
     * @param \ImmoBundle\Entity\Personnel $personnel
     *
     * @return Fonction
     */
    public function setPersonnel(\ImmoBundle\Entity\Personnel $personnel = null)
    {
        $this->personnel = $personnel;

        return $this;
    }

    /**
     * Get personnel
     *
     * @return \ImmoBundle\Entity\Personnel
     */
    public function getPersonnel()
    {
        return $this->personnel;
    }

    function __toString()
    {
        return $this->getNom();
    }


}
