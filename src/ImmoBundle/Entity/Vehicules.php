<?php

namespace ImmoBundle\Entity;

/**
 * Vehicules
 */
class Vehicules
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $immatriculation;

    /**
     * @var \DateTime
     */
    private $assuranceDD;

    /**
     * @var \DateTime
     */
    private $assuranceDF;

    /**
     * @var \DateTime
     */
    private $visiteTechniqueDD;

    /**
     * @var \DateTime
     */
    private $visiteTechniqueDF;

    /**
     * @var \DateTime
     */
    private $permisBleuDD;

    /**
     * @var \DateTime
     */
    private $permisBleuDF;

    /**
     * @var \DateTime
     */
    private $taxeDD;

    /**
     * @var \DateTime
     */
    private $taxeDF;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set immatriculation
     *
     * @param string $immatriculation
     *
     * @return Vehicules
     */
    public function setImmatriculation($immatriculation)
    {
        $this->immatriculation = $immatriculation;

        return $this;
    }

    /**
     * Get immatriculation
     *
     * @return string
     */
    public function getImmatriculation()
    {
        return $this->immatriculation;
    }

    /**
     * Set assuranceDD
     *
     * @param \DateTime $assuranceDD
     *
     * @return Vehicules
     */
    public function setAssuranceDD($assuranceDD)
    {
        $this->assuranceDD = $assuranceDD;

        return $this;
    }

    /**
     * Get assuranceDD
     *
     * @return \DateTime
     */
    public function getAssuranceDD()
    {
        return $this->assuranceDD;
    }

    /**
     * Set assuranceDF
     *
     * @param \DateTime $assuranceDF
     *
     * @return Vehicules
     */
    public function setAssuranceDF($assuranceDF)
    {
        $this->assuranceDF = $assuranceDF;

        return $this;
    }

    /**
     * Get assuranceDF
     *
     * @return \DateTime
     */
    public function getAssuranceDF()
    {
        return $this->assuranceDF;
    }

    /**
     * Set visiteTechniqueDD
     *
     * @param \DateTime $visiteTechniqueDD
     *
     * @return Vehicules
     */
    public function setVisiteTechniqueDD($visiteTechniqueDD)
    {
        $this->visiteTechniqueDD = $visiteTechniqueDD;

        return $this;
    }

    /**
     * Get visiteTechniqueDD
     *
     * @return \DateTime
     */
    public function getVisiteTechniqueDD()
    {
        return $this->visiteTechniqueDD;
    }

    /**
     * Set visiteTechniqueDF
     *
     * @param \DateTime $visiteTechniqueDF
     *
     * @return Vehicules
     */
    public function setVisiteTechniqueDF($visiteTechniqueDF)
    {
        $this->visiteTechniqueDF = $visiteTechniqueDF;

        return $this;
    }

    /**
     * Get visiteTechniqueDF
     *
     * @return \DateTime
     */
    public function getVisiteTechniqueDF()
    {
        return $this->visiteTechniqueDF;
    }

    /**
     * Set permisBleuDD
     *
     * @param \DateTime $permisBleuDD
     *
     * @return Vehicules
     */
    public function setPermisBleuDD($permisBleuDD)
    {
        $this->permisBleuDD = $permisBleuDD;

        return $this;
    }

    /**
     * Get permisBleuDD
     *
     * @return \DateTime
     */
    public function getPermisBleuDD()
    {
        return $this->permisBleuDD;
    }

    /**
     * Set permisBleuDF
     *
     * @param \DateTime $permisBleuDF
     *
     * @return Vehicules
     */
    public function setPermisBleuDF($permisBleuDF)
    {
        $this->permisBleuDF = $permisBleuDF;

        return $this;
    }

    /**
     * Get permisBleuDF
     *
     * @return \DateTime
     */
    public function getPermisBleuDF()
    {
        return $this->permisBleuDF;
    }

    /**
     * Set taxeDD
     *
     * @param \DateTime $taxeDD
     *
     * @return Vehicules
     */
    public function setTaxeDD($taxeDD)
    {
        $this->taxeDD = $taxeDD;

        return $this;
    }

    /**
     * Get taxeDD
     *
     * @return \DateTime
     */
    public function getTaxeDD()
    {
        return $this->taxeDD;
    }

    /**
     * Set taxeDF
     *
     * @param \DateTime $taxeDF
     *
     * @return Vehicules
     */
    public function setTaxeDF($taxeDF)
    {
        $this->taxeDF = $taxeDF;

        return $this;
    }

    /**
     * Get taxeDF
     *
     * @return \DateTime
     */
    public function getTaxeDF()
    {
        return $this->taxeDF;
    }
    /**
     * @var string
     */
    private $model;


    /**
     * Set model
     *
     * @param string $model
     *
     * @return Vehicules
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }
}
