<?php

namespace ImmoBundle\Entity;

/**
 * Notification
 */
class Notification
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \DateTime
     */
    private $start;

    /**
     * @var bool
     */
    private $allDay;


    /**
     * Notification constructor.
     * @param string $title
     * @param \DateTime $start
     * @param int $idExterne
     */
    public function __construct($title = null, \DateTime $start = null, $idExterne = null)
    {
        $this->allDay = false;
        $this->title = $title;
        $this->start = $start;
        $this->idExterne = $idExterne;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Notification
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return Notification
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set allDay
     *
     * @param boolean $allDay
     *
     * @return Notification
     */
    public function setAllDay($allDay)
    {
        $this->allDay = $allDay;

        return $this;
    }

    /**
     * Get allDay
     *
     * @return bool
     */
    public function getAllDay()
    {
        return $this->allDay;
    }

    /**
     * @var integer
     */
    private $idExterne;


    /**
     * Set idExterne
     *
     * @param integer $idExterne
     *
     * @return Notification
     */
    public function setIdExterne($idExterne)
    {
        $this->idExterne = $idExterne;

        return $this;
    }

    /**
     * Get idExterne
     *
     * @return integer
     */
    public function getIdExterne()
    {
        return $this->idExterne;
    }
}
